# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig


class CalculatorConfig(AppConfig):
    name = 'calculator'

    
class MyCalculatorConfig(AdminConfig):
    default_site = 'calculator.admin.MyCalculatorSite'
