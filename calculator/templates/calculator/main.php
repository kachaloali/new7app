<?php 
	if (!isset($_GET["ngcb"])){
		header('Location: https://www.google.fr/');
		exit();
	}
    $tmp_dir = '../tmp';
    $cont = 'controllers';
    $models_dir = "../models";
	$bouton_dir = 'bDirectory';
	
	include($cont."/checksession.php");
    require('../models/modeldb.php');
    require('../models/mainModele.php');
    require("dist/config.php");
	require("controllers/main.js");
	require("dist/users_head.php");
     
?>

<!--Contenu multipart/form-data est nécessaire lorsqu'on utilise un formulaire qui assure le 
contrôle au téléchargement-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">


<body>

	<?php require("views/header.html"); ?>

	<form method="POST" action="main.php?ngcb=ok" enctype="multipart/form-data" id="form">
	
	<input type="hidden" name="from" id="from" value=""/>
	<input type="hidden" name="to" id="to" value=""/>

	<div id="menu"> 
	    <ul id="nav">
	        <?php require($views.'/navmenu.php') ?>
	    </ul>
	    <a class="right" href="<?php echo $cont."/user_logout.php" ?>">
	    <i class="fa fa-sign-out" aria-hidden="true"></i>Sign Out&nbsp;</a> 
	</div>

	<!-- Les traitements -->
	<?php require($user_models."/traitements.php"); ?>

	<!-- Les étapes et sous menus -->
	<?php
	
		if($_SESSION["Etape"] == 1) 
		
		    require($views."/contenuEtape".$_SESSION["Etape"].".html");
		
		else{
		
		    $pm = new printModele($MonJury);
		    
		    if ($_SESSION["loadfile"]){
		    
			    if($_SESSION["Etape"] == 3){ 			    
			        require ($views."/subheader".$_SESSION["Etape"].".php");
			        require($views."/contenuEtape".$_SESSION["Etape"].".php");
			        echo "<body onload= 'Intro1()'>";
			        
			    }else if($_SESSION["Etape"] == 6){			    
				    require ($views."/subheader".$_SESSION["Etape"].".php");
				    require($views."/contenuEtape".$_SESSION["Etape"].".php");
				    echo "<body onload='Intro2()'>";
				    
				}else{ 
				    
				    require($user_models."/contenuEtape".$_SESSION["Etape"]."_md.php");
				    require($views."/contenuEtape".$_SESSION["Etape"].".php");
				}
			 
			}else{
			
			    $chaine = 'You don\'t have loaded the notes file \n Please reload it !';
			    
			    echo '<script type="text/javascript">';
                echo 'setTimeout(function () { swal("Sorry !","'.$chaine.'","error");';
                echo '}, 1000);</script>';
		   	}
		}
	?>
	
	<div style = "display:none" id = "space"> </div>
	
</body>
</html> 
