<?php

	$nometapes = array(
	    "Notes File","Disciplines and Coef.","1<sup>st</sup> Group Results","Choice of Disciplines",
	    "2<sup>nd</sup> Group Notes","2<sup>nd</sup> group Results" 
	);
	
	$infosbulles = array("",
	    "Allows to fill in the disciplines and their coefficients",
	    "Shows the candidates who are admitted at the outcome of the 1st group tests",
	    "Allows to check the subjects of the candidates authorized to pass the second group tests",
	    "Allows to fill in the disciplines of the candidates having passed the second group tests",
	    "Shows the candidates who are admitted at the outcome of the second group tests"
	);

	for($i = 0; $i < count($nometapes); $i++) {
	
		if ($i+1 == $_SESSION["Etape"]) {
			
			$chaine = '<li><a id="etape" href = "javascript:soumettre("'.$_SESSION["Etape"];
			$chaine .= ','.($i+1).')">'.$nometapes[$i].'</a> </li>';
			echo $chaine;
			
		}else{
		
			$chaine = '<li> <a href = "javascript:soumettre('.$_SESSION["Etape"].','.($i+1);
			$chaine .= ')" title = "'.$infosbulles[$i].'">'.$nometapes[$i].'</a> </li>';
			echo $chaine;
		}
	}
	
?>
