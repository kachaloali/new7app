# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Admin(models.Model):
    lastname = models.CharField(max_length=50)
    firstname = models.CharField(max_length=50)
    login = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'admin'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Centre(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nom = models.CharField(max_length=50)
    region = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'centre'


class Coefficient(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    codeserie = models.ForeignKey('Serie', models.DO_NOTHING, db_column='codeserie')
    codemat = models.CharField(max_length=10)
    coefmat = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'coefficient'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Jury(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nojury = models.IntegerField()
    centre = models.CharField(max_length=50)
    region = models.ForeignKey('Region', models.DO_NOTHING, db_column='region')

    class Meta:
        managed = False
        db_table = 'jury'


class Matiere(models.Model):
    codemat = models.CharField(primary_key=True, max_length=10)
    matiere = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'matiere'


class Point(models.Model):
    groupe_1 = models.CharField(db_column='Groupe_1', max_length=10)  # Field name made lowercase.
    groupe_2 = models.CharField(db_column='Groupe_2', max_length=10)  # Field name made lowercase.
    codeserie = models.ForeignKey('Serie', models.DO_NOTHING, db_column='codeserie')

    class Meta:
        managed = False
        db_table = 'point'


class Region(models.Model):
    nameregion = models.CharField(primary_key=True, max_length=50)

    class Meta:
        managed = False
        db_table = 'region'


class Serie(models.Model):
    codeserie = models.CharField(primary_key=True, max_length=10)

    class Meta:
        managed = False
        db_table = 'serie'


class Student(models.Model):
    anonymat = models.IntegerField()
    notable = models.IntegerField(primary_key=True)
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    datenaiss = models.CharField(max_length=20)
    lieunaiss = models.CharField(max_length=50)
    nationalite = models.CharField(max_length=20)
    sexe = models.CharField(max_length=10)
    etablissement = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'student'


class Typescol(models.Model):
    toustypes = models.CharField(max_length=500)
    entetes = models.CharField(max_length=500)
    matieres = models.CharField(max_length=500)

    class Meta:
        managed = False
        db_table = 'typescol'


class User(models.Model):
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    login = models.CharField(primary_key=True, max_length=50)
    password = models.CharField(max_length=50)
    region = models.ForeignKey(Region, models.DO_NOTHING, db_column='region')
    numero = models.IntegerField()
    centre = models.CharField(max_length=50)
    codeserie = models.ForeignKey(Serie, models.DO_NOTHING, db_column='codeserie')

    class Meta:
        managed = False
        db_table = 'user'
