# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.admin import AdminSite



class MyAdminSite(AdminSite):
    site_header = 'Calculator Administration'
    index_title = 'Calculette'
    site_title = " "
    #index_template = 'calculator/admins.html'

admin_site = MyAdminSite(name='myadmin')



# Register your models here.
from calculator.models import *
admin_site.register(Admin)
admin_site.register(Centre)
admin_site.register(Coefficient)
admin_site.register(Jury)
admin_site.register(Region)
admin_site.register(Serie)
admin_site.register(Matiere)
admin_site.register(Student)
admin_site.register(User)
admin_site.register(Typescol)
admin_site.register(Point)

