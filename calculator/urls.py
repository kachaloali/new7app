from calculator.views import *
from django.conf import settings
from django.conf.urls import url
from django.views.generic import RedirectView
from django.views.generic import TemplateView
from calculator.admin import admin_site

#from django.contrib import admin
urlpatterns = [
	url('admin/', admin_site.urls),

	#url(r'^$', home.as_view(), name='home'),
	url(r'^$', home, name = "home"),
	url(r'^users/$', users, name = "users"),
	url(r'^admins/$', admins, name = "admins"),
	url(r'^helps/$', helps, name = "helps"),
	#url(r'^main/$', main, name = "main"),
	url(r'^main/login-success/$', main, name = "act1"),
	url(r'^main/upload-ok/$', upload, name = "upload"),
	url(r'^main/step-2-ok/$', process, name = "step-2"),
	url(r'^main/logout/$', logout, name = "logout"),
]
