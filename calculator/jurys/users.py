class eleve():
	def __init__(self):
		self.entetes = dict()
		self.notes 	 = list()
		self.etat 	 = str()

class matiere():
	def __init__(self, mat, grade, coef, _type):
		self.matiere = mat
		self.grade	 = grade
		self.coeff   = coef
		self.type    = _type

class Jury():
	def __init__(self, userID, filename):
		self.id = userID
		self.filename = filename
		self.dialect = ""
		self.entetes = self.getEntetes()
		self.promo = list()
	
	def readCSV(self):
		import csv
		csvfile = open(self.filename, "rb")
		dialect = csv.Sniffer().sniff(csvfile.read(1024))
		csvfile.seek(0)
		self.dialect = dialect
		return csv.reader(csvfile, dialect)
	
	def getEntetes(self):
		mylist = next(self.readCSV())
		return {e: str(mylist.index(e)) for e in list(filter(None, mylist))}
		
 	def getPromo(self):
 		reader = self.readCSV()
 		for row in reader[1:]:
 			line = row.split(dialect)
 			
