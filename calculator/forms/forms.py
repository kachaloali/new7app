# -*- coding: utf-8 -*
from django import forms
from calculator.models import Serie

class usersForm(forms.Form):
	SERIES = Serie.objects.all()
	CHOICES = list()
	CHOICES.append(("", 'Veuillez sélectionner une série'))
	for serie in SERIES:
		CHOICES.append((serie.codeserie, serie.codeserie))
	CHOICES = tuple(CHOICES)
	
	userName = forms.CharField(label='Login')
	password = forms.CharField(label='Pass', widget=forms.PasswordInput())
	serie = forms.ChoiceField(choices = CHOICES, )
	
