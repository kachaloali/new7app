# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.template import loader
from django.views.generic.base import TemplateView
from django.shortcuts import redirect
from django.views.decorators.cache import never_cache
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse



# Create your views here.
import os, shutil, ast
from django.http import *
from datetime import datetime
from calculator.forms.forms import usersForm
from calculator.models import Serie
from calculator.models import User
from calculator.models import Typescol
from calculator.models import Point
from calculator.models import Coefficient
from calculator.jurys.users import Jury


#class home(TemplateView):
#	template_name = 'calculator/usersLogin.html'
#	def get_context_data(self, **kwargs):
#		context = super(home, self).get_context_data(**kwargs)
#		context['SERIES'] = Series.objects.all()
#		context['FORM'] = usersForm()
#		return context


def start(request):
	userID = request.session.get('userID')
	if not userID: userID = ""
	request.session['userID'] = userID
	return userID

@csrf_protect
def home(request):
	template = 'calculator/usersLogin.html'
	return render(request, template, {"FORM": usersForm()})

def users(request):
	SERIES = Series.objects.all()
	return render(request, 'calculator/usersLogin.html', {'SERIES': SERIES})
 
def admins(request):
	return render(request, 'calculator/admins.html')
	
def helps(request):
	return render(request, 'calculator/helps.html')


@csrf_protect 
#@never_cache	
def main(request):
	if request.method == 'POST':
		form = usersForm(request.POST)
		if form.is_valid():
			log = form.cleaned_data['userName']
			pas = form.cleaned_data['password']
			serie = form.cleaned_data['serie']
			
			user = User.objects.filter(login = log).get()
			if user:# and user.password == pas and user.codeserie.codeserie == serie:
				request.session["USERID"] = log
				request.session["SERIE"] = serie
				request.session["AUTHENTICATED"] = True
				request.session['STEP'] = 1
			elif not user:
				request.session["AUTHENTICATED"] = False
				message = 'Vous n\'êtes pas identifié.'+ serie
			elif user.password != pas:
				request.session["AUTHENTICATED"] = False
				message = 'Vous avez renseigné mauvais mot de passe.'+ user.password + pas
			elif user.codeserie.codeserie != serie:
				request.session["AUTHENTICATED"] = False
				message = 'Vous n\'êtes pas autorité à traiter la serie '+serie
			
			if request.session["AUTHENTICATED"]:
				return render(request, 'calculator/main.html')
			else:
				form = usersForm()
				return render(request, 'calculator/usersLogin.html', 
							{'FORM': form, "MESSAGE": message})
		else: redirect("/")


@csrf_protect
def logout(request):
    try:
    	userID = request.session.get('USERID')
    	path = os.path.join('calculator', 'tmp', userID)
    	if os.path.exists(path): 
    		shutil.rmtree(path, ignore_errors=True)
    	for key in request.session.keys(): del request.session[key]
    except KeyError: pass
    return redirect('/')

def readCSV(filename):
	import csv
	csvfile = open(filename, "rb")
	dialect = csv.Sniffer().sniff(csvfile.read(1024))
	csvfile.seek(0)
	return csv.reader(csvfile, dialect)

def getEntetes(filename):
	mylist = next(readCSV(filename))
	return {e: str(mylist.index(e)) for e in list(filter(None, mylist))}
		
		
def upload(request):
	userID = request.session.get('USERID')
	serie = request.session.get('SERIE')
	if request.method == 'POST' and request.FILES['upfile'] and userID:
		path = os.path.join('calculator', 'tmp', userID)
		if not os.path.exists(path): os.makedirs(path)
		myfile = request.FILES['upfile']
		fs = FileSystemStorage(location=path)
		filename = fs.url(fs.save(myfile.name, myfile))
		filename = os.path.join(path, filename)
		request.session["UPFILE"] = filename
		request.session["UPLOADED"] = True
		request.session['STEP'] = 2
		
		###############################################################################
		coefficients = Coefficient.objects.filter(codeserie=serie)
		codes = [coef.codemat for coef in coefficients]
		coeffs = {coef.codemat:coef.coefmat for coef in coefficients}
		
		dicHeader = getEntetes(filename)
		typescol = Typescol.objects.all().get()
		lst = typescol.toustypes.replace('\n','').split(';')
		header = typescol.entetes.replace('\n','').split(';')
		tousTypes = { e: str(lst.index(e)) for e in lst }
		typesHeader = ["type_"+ str(tousTypes[el]) for el in tousTypes if el in header ]
		typesMatter = ["type_"+ str(tousTypes[e]) for e in tousTypes if e not in header]
		
		points = Point.objects.filter(codeserie=serie).get()
		return render(request, 
						'calculator/main.html', {
						'HEADER0'	 : dict(dicHeader.items()[len(dicHeader)/2:]), 
						'HEADER1'    : dict(dicHeader.items()[:len(dicHeader)/2]), 
						'TYPESCOLS'  : tousTypes,
						'CODES'		 : codes,
						'COEFFS'	 : coeffs,
						'TYPESHEADER': typesHeader,
						'TYPESMATTER': typesMatter,
						'POINTS1GRPE': points.groupe_1,
						'POINTS2GRPE': points.groupe_2
						}
		)
	else:
		request.session["UPLOADED"] = False
		return render(request, 'calculator/main.html')

def getTypes(Types, row):
	types = dict()
	for elt in ast.literal_eval(Types):
		num = int(elt.split('_')[1].strip("'"))
		types[num] = row[num]
	return types

	
def process(request):
	if request.method == 'POST':
		request.session['STEP'] = 3
		serie = request.session['SERIE']
		typesHeader = request.POST['typesHeader']
		typesMatter = request.POST['typesMatter']
		
		coefficients = Coefficient.objects.filter(codeserie=serie)
		coeffs = {str(obj.codemat):str(obj.coefmat) for obj in coefficients}
		
		step2data, typescodes = dict(), dict()
		filename = request.session['UPFILE']
		dicHeader = getEntetes(filename)
		myDicHeader = {v: k for k, v in dicHeader.items()}
		for key, val in dicHeader.items():
			_type = request.POST['col_'+ val]
			step2data[str(val)] = [str(_type)]
			
			if _type in ast.literal_eval(typesMatter):
				code = str(request.POST['code_'+ val])
				typescodes[code] = str(_type)
				step2data[str(val)].append(code)
				step2data[str(val)].append(coeffs[code])
			else:
				step2data[str(val)].append(myDicHeader[str(val)])
		
		
		reader = readCSV(filename)
		next(reader, None)
		i=0
		headers, matters = dict(), dict()
		for row in reader:
			headers[i] = getTypes(typesHeader, row)
			matters[i] = getTypes(typesMatter, row)
			i+= 1
		
		def computeTotalPoints(step2data, mat):
			total = 0
			for num, note in mat.items():
				data = step2data[str(num)]
				if data[1] != "EPS" and data[1] != "LV2":
					total += float(note.replace(',','.')) * float(data[2])
			return total
		
		totaux = dict()
		for numCol, data in matters.items():
			totaux[numCol] = computeTotalPoints(step2data, data)
		
		for num, header in headers.items():
			headers[num][len(header)] = totaux[num]
		
		
		points = Point.objects.filter(codeserie=serie).get()
		admis1grpe, admis2grpe, refuses1grpe = dict(), dict(), dict()
		for num, infos in headers.items():
			if infos[len(infos)-1] >= float(points.groupe_1):
				admis1grpe[num] =  infos
			elif infos[len(infos)-1] >= float(points.groupe_2):
				admis2grpe[num] =  infos
			else:
				refuses1grpe[num] =  infos
		
		return render(request, 'calculator/main.html', 
			{ 
				'dicHeader': dicHeader,
				'STEP2DATA': step2data,
				'TYPESCODES': typescodes,
				'COEFFS': coeffs,
				'HEADERS': headers,
				'MATTERS': matters,
				'ADMIS1GRPE': admis1grpe,
				'ADMIS2GRPE': admis2grpe,
				'REFUSES1GRPE': refuses1grpe
				
			})
