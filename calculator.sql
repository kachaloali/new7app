-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: calculator
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `login` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('Kachalo','Ali','admin','2018',1);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add permission',3,'add_permission'),(8,'Can change permission',3,'change_permission'),(9,'Can delete permission',3,'delete_permission'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add admin',7,'add_admin'),(20,'Can change admin',7,'change_admin'),(21,'Can delete admin',7,'delete_admin'),(22,'Can add user',8,'add_user'),(23,'Can change user',8,'change_user'),(24,'Can delete user',8,'delete_user'),(25,'Can add matiere',9,'add_matiere'),(26,'Can change matiere',9,'change_matiere'),(27,'Can delete matiere',9,'delete_matiere'),(28,'Can add student',10,'add_student'),(29,'Can change student',10,'change_student'),(30,'Can delete student',10,'delete_student'),(31,'Can add coefficient',11,'add_coefficient'),(32,'Can change coefficient',11,'change_coefficient'),(33,'Can delete coefficient',11,'delete_coefficient'),(34,'Can add region',12,'add_region'),(35,'Can change region',12,'change_region'),(36,'Can delete region',12,'delete_region'),(37,'Can add centre',13,'add_centre'),(38,'Can change centre',13,'change_centre'),(39,'Can delete centre',13,'delete_centre'),(40,'Can add jury',14,'add_jury'),(41,'Can change jury',14,'change_jury'),(42,'Can delete jury',14,'delete_jury'),(43,'Can add serie',15,'add_serie'),(44,'Can change serie',15,'change_serie'),(45,'Can delete serie',15,'delete_serie'),(46,'Can add csv header',16,'add_csvheader'),(47,'Can change csv header',16,'change_csvheader'),(48,'Can delete csv header',16,'delete_csvheader'),(49,'Can add csv all cols',17,'add_csvallcols'),(50,'Can change csv all cols',17,'change_csvallcols'),(51,'Can delete csv all cols',17,'delete_csvallcols'),(52,'Can add points',18,'add_points'),(53,'Can change points',18,'change_points'),(54,'Can delete points',18,'delete_points'),(55,'Can add types cols',19,'add_typescols'),(56,'Can change types cols',19,'change_typescols'),(57,'Can delete types cols',19,'delete_typescols');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$36000$6lmoDX7vGCAV$tbAs/MwjW7cAcEiCAs/VWOkgXa20ovZI1Xe/iSVelrc=','2018-11-05 08:27:31.458570',1,'kachaloali','','','hka2r89@gmail.com',1,1,'2018-11-04 00:49:38.747674');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point`
--

DROP TABLE IF EXISTS `point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Groupe_1` varchar(10) NOT NULL,
  `Groupe_2` varchar(10) NOT NULL,
  `codeserie` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `point_codeserie_48490a58_fk_serie_codeserie` (`codeserie`),
  CONSTRAINT `point_codeserie_48490a58_fk_serie_codeserie` FOREIGN KEY (`codeserie`) REFERENCES `serie` (`codeserie`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point`
--

LOCK TABLES `point` WRITE;
/*!40000 ALTER TABLE `point` DISABLE KEYS */;
INSERT INTO `point` VALUES (3,'150','120','A4'),(4,'160','140','D');
/*!40000 ALTER TABLE `point` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typescol`
--

DROP TABLE IF EXISTS `typescol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typescol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `toustypes` varchar(1000) NOT NULL,
  `entetes` varchar(600) NOT NULL,
  `matieres` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typescol`
--

LOCK TABLES `typescol` WRITE;
/*!40000 ALTER TABLE `typescol` DISABLE KEYS */;
INSERT INTO `typescol` VALUES (1, 
                              'N° Anonymat;
                               N° de Table;
                               Établissement;
                               Nom;
                               Prénoms;
                               Sexe; 
                               Date de Naiss.; 
                               Lieu de Naiss.;
                               Nationalité;
                               Mat. 1er Grpe; 
                               Mat. 2nd Grpe;
                               Mat. 1/2nd Grpe;
                               Edu. Sportive;
                               Mat. Optionnelle',
                              'N° Anonymat;
                               N° de Table;
                               Établissement;
                               Nom;
                               Prénoms;
                               Sexe; 
                               Date de Naiss.; 
                               Lieu de Naiss.;
                               Nationalité',
                              'Mat. 1er Grpe; 
                               Mat. 2nd Grpe;
                               Mat. 1/2nd Grpe;
                               Edu. Sportive;
                               Mat. Optionnelle'
                              );
/*!40000 ALTER TABLE `typescol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centre`
--

DROP TABLE IF EXISTS `centre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centre` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `region` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centre`
--

LOCK TABLES `centre` WRITE;
/*!40000 ALTER TABLE `centre` DISABLE KEYS */;
INSERT INTO `centre` VALUES (1,'LDB','Maradi'),(2,'Franco-Arabe','Maradi'),(3,'CEG1','Maradi'),(4,'Dan Koulodo','Maradi'),(5,'Bourja','Maradi'),(6,'Illimi','Maradi'),(7,'Adbou Moumouni 1','Niamey'),(8,'Adbou Moumouni 2','Niamey'),(9,'Adbou Moumouni 3','Niamey'),(10,'Adbou Moumouni 4','Niamey'),(11,'Adbou Moumouni 5','Niamey'),(12,'Zinder 1','Zinder'),(13,'Zinder 2','Zinder'),(14,'Zinder 3','Zinder'),(15,'Zinder 4','Zinder'),(16,'Zinder 5','Zinder'),(17,'Tahoua 1','Tahoua'),(18,'Tahoua 2','Tahoua'),(19,'Tahoua 3','Tahoua'),(20,'Tahoua 4','Tahoua'),(21,'Tahoua 5','Tahoua'),(22,'Dosso 1','Dosso'),(23,'Dosso 2','Dosso'),(24,'Dosso 3','Dosso'),(25,'Dosso 4','Dosso'),(26,'Dosso 5','Dosso'),(27,'Diffa 1','Diffa'),(28,'Diffa 2','Diffa'),(29,'Diffa 3','Diffa'),(30,'Diffa 4','Diffa'),(31,'Diffa 5','Diffa'),(32,'Tillaberi 1','Tillaberi'),(33,'Tillaberi 2','Tillaberi'),(34,'Tillaberi 3','Tillaberi'),(35,'Tillaberi 4','Tillaberi'),(36,'Tillaberi 5','Tillaberi'),(37,'Agadez 1','Agadez'),(38,'Agadez 2','Agadez'),(39,'Agadez 3','Agadez'),(40,'Agadez 4','Agadez'),(41,'Agadez 5','Agadez');
/*!40000 ALTER TABLE `centre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=347 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2018-11-04 10:23:25.112454','306','LV1',3,'',17,1),(2,'2018-11-04 10:23:25.228681','305','LV2',3,'',17,1),(3,'2018-11-04 10:23:25.306750','304','PH',3,'',17,1),(4,'2018-11-04 10:23:25.474113','303','Des/cout',3,'',17,1),(5,'2018-11-04 10:23:25.518511','302','EPS',3,'',17,1),(6,'2018-11-04 10:23:25.562931','301','HG',3,'',17,1),(7,'2018-11-04 10:23:25.607126','300','MA',3,'',17,1),(8,'2018-11-04 10:23:25.652102','299','FR',3,'',17,1),(9,'2018-11-04 10:23:25.696597','298','NATIONALITE',3,'',17,1),(10,'2018-11-04 10:23:25.740797','297','SEXE',3,'',17,1),(11,'2018-11-04 10:23:25.785616','296','PRENOMS',3,'',17,1),(12,'2018-11-04 10:23:25.829800','295','LIEU DE NAIS.',3,'',17,1),(13,'2018-11-04 10:23:25.874328','294','DATE NAIS.',3,'',17,1),(14,'2018-11-04 10:23:25.919261','293','No  DE TABLE',3,'',17,1),(15,'2018-11-04 10:23:25.963626','292','No  ANONYMAT  1er GROUPE',3,'',17,1),(16,'2018-11-04 10:23:26.007924','291','NOM',3,'',17,1),(17,'2018-11-04 10:23:26.043286','290','ETABLISSEMENT',3,'',17,1),(18,'2018-11-04 10:23:26.087812','289','LV1',3,'',17,1),(19,'2018-11-04 10:23:26.132551','288','LV2',3,'',17,1),(20,'2018-11-04 10:23:26.187331','287','PH',3,'',17,1),(21,'2018-11-04 10:23:26.270801','286','Des/cout',3,'',17,1),(22,'2018-11-04 10:23:26.344010','285','EPS',3,'',17,1),(23,'2018-11-04 10:23:26.388511','284','HG',3,'',17,1),(24,'2018-11-04 10:23:26.433149','283','MA',3,'',17,1),(25,'2018-11-04 10:23:26.477558','282','FR',3,'',17,1),(26,'2018-11-04 10:23:26.522339','281','NATIONALITE',3,'',17,1),(27,'2018-11-04 10:23:26.566792','280','SEXE',3,'',17,1),(28,'2018-11-04 10:23:26.611294','279','PRENOMS',3,'',17,1),(29,'2018-11-04 10:23:26.644629','278','LIEU DE NAIS.',3,'',17,1),(30,'2018-11-04 10:23:26.690156','277','DATE NAIS.',3,'',17,1),(31,'2018-11-04 10:23:26.722489','276','No  DE TABLE',3,'',17,1),(32,'2018-11-04 10:23:26.755917','275','No  ANONYMAT  1er GROUPE',3,'',17,1),(33,'2018-11-04 10:23:26.789271','274','NOM',3,'',17,1),(34,'2018-11-04 10:23:26.822846','273','ETABLISSEMENT',3,'',17,1),(35,'2018-11-04 10:23:26.856344','272','LV1',3,'',17,1),(36,'2018-11-04 10:23:26.889659','271','LV2',3,'',17,1),(37,'2018-11-04 10:23:26.923361','270','PH',3,'',17,1),(38,'2018-11-04 10:23:26.956771','269','Des/cout',3,'',17,1),(39,'2018-11-04 10:23:26.989860','268','EPS',3,'',17,1),(40,'2018-11-04 10:23:27.023361','267','HG',3,'',17,1),(41,'2018-11-04 10:23:27.056843','266','MA',3,'',17,1),(42,'2018-11-04 10:23:27.090557','265','FR',3,'',17,1),(43,'2018-11-04 10:23:27.123987','264','NATIONALITE',3,'',17,1),(44,'2018-11-04 10:23:27.157462','263','SEXE',3,'',17,1),(45,'2018-11-04 10:23:27.313503','262','PRENOMS',3,'',17,1),(46,'2018-11-04 10:23:27.346629','261','LIEU DE NAIS.',3,'',17,1),(47,'2018-11-04 10:23:27.379928','260','DATE NAIS.',3,'',17,1),(48,'2018-11-04 10:23:27.413342','259','No  DE TABLE',3,'',17,1),(49,'2018-11-04 10:23:27.447010','258','No  ANONYMAT  1er GROUPE',3,'',17,1),(50,'2018-11-04 10:23:27.480376','257','NOM',3,'',17,1),(51,'2018-11-04 10:23:27.513824','256','ETABLISSEMENT',3,'',17,1),(52,'2018-11-04 10:23:27.547311','255','LV1',3,'',17,1),(53,'2018-11-04 10:23:27.580786','254','LV2',3,'',17,1),(54,'2018-11-04 10:23:27.614231','253','PH',3,'',17,1),(55,'2018-11-04 10:23:27.658624','252','Des/cout',3,'',17,1),(56,'2018-11-04 10:23:27.692289','251','EPS',3,'',17,1),(57,'2018-11-04 10:23:27.725285','250','HG',3,'',17,1),(58,'2018-11-04 10:23:27.758528','249','MA',3,'',17,1),(59,'2018-11-04 10:23:27.792157','248','FR',3,'',17,1),(60,'2018-11-04 10:23:27.825784','247','NATIONALITE',3,'',17,1),(61,'2018-11-04 10:23:27.870783','246','SEXE',3,'',17,1),(62,'2018-11-04 10:23:27.948191','245','PRENOMS',3,'',17,1),(63,'2018-11-04 10:23:27.981150','244','LIEU DE NAIS.',3,'',17,1),(64,'2018-11-04 10:23:28.014520','243','DATE NAIS.',3,'',17,1),(65,'2018-11-04 10:23:28.049849','242','No  DE TABLE',3,'',17,1),(66,'2018-11-04 10:23:28.093145','241','No  ANONYMAT  1er GROUPE',3,'',17,1),(67,'2018-11-04 10:23:28.137349','240','NOM',3,'',17,1),(68,'2018-11-04 10:23:28.259922','239','ETABLISSEMENT',3,'',17,1),(69,'2018-11-04 10:23:28.292936','238','Colonne_11',3,'',17,1),(70,'2018-11-04 10:23:28.326735','237','Colonne_14',3,'',17,1),(71,'2018-11-04 10:23:28.371307','236','Colonne_10',3,'',17,1),(72,'2018-11-04 10:23:28.437778','235','Colonne_16',3,'',17,1),(73,'2018-11-04 10:23:28.482481','234','Colonne_15',3,'',17,1),(74,'2018-11-04 10:23:28.526687','233','Colonne_12',3,'',17,1),(75,'2018-11-04 10:23:28.560254','232','Colonne_13',3,'',17,1),(76,'2018-11-04 10:23:28.593972','231','Colonne_9',3,'',17,1),(77,'2018-11-04 10:23:28.638774','230','Colonne_8',3,'',17,1),(78,'2018-11-04 10:23:28.683035','229','Colonne_5',3,'',17,1),(79,'2018-11-04 10:23:28.727178','228','Colonne_4',3,'',17,1),(80,'2018-11-04 10:23:28.772318','227','Colonne_7',3,'',17,1),(81,'2018-11-04 10:23:28.816248','226','Colonne_6',3,'',17,1),(82,'2018-11-04 10:23:28.850259','225','Colonne_1',3,'',17,1),(83,'2018-11-04 10:23:28.894491','224','Colonne_0',3,'',17,1),(84,'2018-11-04 10:23:28.939182','223','Colonne_3',3,'',17,1),(85,'2018-11-04 10:23:28.983551','222','Colonne_2',3,'',17,1),(86,'2018-11-04 10:23:29.027985','221','Colonne_11',3,'',17,1),(87,'2018-11-04 10:23:29.072412','220','Colonne_14',3,'',17,1),(88,'2018-11-04 10:23:29.117132','219','Colonne_10',3,'',17,1),(89,'2018-11-04 10:23:29.190243','218','Colonne_16',3,'',17,1),(90,'2018-11-04 10:23:29.239823','217','Colonne_15',3,'',17,1),(91,'2018-11-04 10:23:29.283915','216','Colonne_12',3,'',17,1),(92,'2018-11-04 10:23:29.328692','215','Colonne_13',3,'',17,1),(93,'2018-11-04 10:23:29.392212','214','Colonne_9',3,'',17,1),(94,'2018-11-04 10:23:29.429039','213','Colonne_8',3,'',17,1),(95,'2018-11-04 10:23:29.473352','212','Colonne_5',3,'',17,1),(96,'2018-11-04 10:23:29.518848','211','Colonne_4',3,'',17,1),(97,'2018-11-04 10:23:29.562192','210','Colonne_7',3,'',17,1),(98,'2018-11-04 10:23:29.606848','209','Colonne_6',3,'',17,1),(99,'2018-11-04 10:23:29.651456','208','Colonne_1',3,'',17,1),(100,'2018-11-04 10:23:29.696381','207','Colonne_0',3,'',17,1),(101,'2018-11-04 10:23:49.925280','206','Colonne_3',3,'',17,1),(102,'2018-11-04 10:23:50.092477','205','Colonne_2',3,'',17,1),(103,'2018-11-04 10:23:50.125279','204','Colonne_11',3,'',17,1),(104,'2018-11-04 10:23:50.158883','203','Colonne_14',3,'',17,1),(105,'2018-11-04 10:23:50.192967','202','Colonne_10',3,'',17,1),(106,'2018-11-04 10:23:50.237103','201','Colonne_16',3,'',17,1),(107,'2018-11-04 10:23:50.270318','200','Colonne_15',3,'',17,1),(108,'2018-11-04 10:23:50.304074','199','Colonne_12',3,'',17,1),(109,'2018-11-04 10:23:50.337710','198','Colonne_13',3,'',17,1),(110,'2018-11-04 10:23:50.370688','197','Colonne_9',3,'',17,1),(111,'2018-11-04 10:23:50.403667','196','Colonne_8',3,'',17,1),(112,'2018-11-04 10:23:50.437281','195','Colonne_5',3,'',17,1),(113,'2018-11-04 10:23:50.470741','194','Colonne_4',3,'',17,1),(114,'2018-11-04 10:23:50.504398','193','Colonne_7',3,'',17,1),(115,'2018-11-04 10:23:50.549100','192','Colonne_6',3,'',17,1),(116,'2018-11-04 10:23:50.593045','191','Colonne_1',3,'',17,1),(117,'2018-11-04 10:23:50.626466','190','Colonne_0',3,'',17,1),(118,'2018-11-04 10:23:50.660192','189','Colonne_3',3,'',17,1),(119,'2018-11-04 10:23:50.705170','188','Colonne_2',3,'',17,1),(120,'2018-11-04 10:23:50.749770','187','Des/cout',3,'',17,1),(121,'2018-11-04 10:23:50.793772','186','EPS',3,'',17,1),(122,'2018-11-04 10:23:50.827356','185','LV2',3,'',17,1),(123,'2018-11-04 10:23:50.872172','184','MA',3,'',17,1),(124,'2018-11-04 10:23:51.005036','183','HG',3,'',17,1),(125,'2018-11-04 10:23:51.039090','182','LV1',3,'',17,1),(126,'2018-11-04 10:23:51.083449','181','PH',3,'',17,1),(127,'2018-11-04 10:23:51.127402','180','FR',3,'',17,1),(128,'2018-11-04 10:23:51.160915','179','NATIONALITE',3,'',17,1),(129,'2018-11-04 10:23:51.205884','178','LIEU DE NAIS.',3,'',17,1),(130,'2018-11-04 10:23:51.250247','177','DATE NAIS.',3,'',17,1),(131,'2018-11-04 10:23:51.294923','176','SEXE',3,'',17,1),(132,'2018-11-04 10:23:51.339256','175','PRENOMS',3,'',17,1),(133,'2018-11-04 10:23:51.417241','174','NOM',3,'',17,1),(134,'2018-11-04 10:23:51.484204','173','ETABLISSEMENT',3,'',17,1),(135,'2018-11-04 10:23:51.528737','172','No  DE TABLE',3,'',17,1),(136,'2018-11-04 10:23:51.573239','171','No  ANONYMAT  1er GROUPE',3,'',17,1),(137,'2018-11-04 10:23:51.617762','170','Des/cout',3,'',17,1),(138,'2018-11-04 10:23:51.692384','169','EPS',3,'',17,1),(139,'2018-11-04 10:23:51.739802','168','LV2',3,'',17,1),(140,'2018-11-04 10:23:51.784707','167','MA',3,'',17,1),(141,'2018-11-04 10:23:51.828966','166','HG',3,'',17,1),(142,'2018-11-04 10:23:51.873706','165','LV1',3,'',17,1),(143,'2018-11-04 10:23:51.918121','164','PH',3,'',17,1),(144,'2018-11-04 10:23:51.962523','163','FR',3,'',17,1),(145,'2018-11-04 10:23:52.007341','162','NATIONALITE',3,'',17,1),(146,'2018-11-04 10:23:52.084916','161','LIEU DE NAIS.',3,'',17,1),(147,'2018-11-04 10:23:52.179431','160','DATE NAIS.',3,'',17,1),(148,'2018-11-04 10:23:52.229889','159','SEXE',3,'',17,1),(149,'2018-11-04 10:23:52.274343','158','PRENOMS',3,'',17,1),(150,'2018-11-04 10:23:52.318647','157','NOM',3,'',17,1),(151,'2018-11-04 10:23:52.363854','156','ETABLISSEMENT',3,'',17,1),(152,'2018-11-04 10:23:52.407450','155','No  DE TABLE',3,'',17,1),(153,'2018-11-04 10:23:52.452137','154','No  ANONYMAT  1er GROUPE',3,'',17,1),(154,'2018-11-04 10:23:52.497512','153','Des/cout',3,'',17,1),(155,'2018-11-04 10:23:52.541565','152','EPS',3,'',17,1),(156,'2018-11-04 10:23:52.586163','151','LV2',3,'',17,1),(157,'2018-11-04 10:23:52.630518','150','MA',3,'',17,1),(158,'2018-11-04 10:23:52.674874','149','HG',3,'',17,1),(159,'2018-11-04 10:23:52.719308','148','LV1',3,'',17,1),(160,'2018-11-04 10:23:52.764146','147','PH',3,'',17,1),(161,'2018-11-04 10:23:52.808678','146','FR',3,'',17,1),(162,'2018-11-04 10:23:52.853369','145','NATIONALITE',3,'',17,1),(163,'2018-11-04 10:23:52.897539','144','LIEU DE NAIS.',3,'',17,1),(164,'2018-11-04 10:23:52.942309','143','DATE NAIS.',3,'',17,1),(165,'2018-11-04 10:23:52.986920','142','SEXE',3,'',17,1),(166,'2018-11-04 10:23:53.031501','141','PRENOMS',3,'',17,1),(167,'2018-11-04 10:23:53.075765','140','NOM',3,'',17,1),(168,'2018-11-04 10:23:53.120305','139','ETABLISSEMENT',3,'',17,1),(169,'2018-11-04 10:23:53.165324','138','No  DE TABLE',3,'',17,1),(170,'2018-11-04 10:23:53.209151','137','No  ANONYMAT  1er GROUPE',3,'',17,1),(171,'2018-11-04 10:23:53.253753','136','Des/cout',3,'',17,1),(172,'2018-11-04 10:23:53.298230','135','EPS',3,'',17,1),(173,'2018-11-04 10:23:53.342814','134','LV2',3,'',17,1),(174,'2018-11-04 10:23:53.387249','133','MA',3,'',17,1),(175,'2018-11-04 10:23:53.431940','132','HG',3,'',17,1),(176,'2018-11-04 10:23:53.493227','131','LV1',3,'',17,1),(177,'2018-11-04 10:23:53.587738','130','PH',3,'',17,1),(178,'2018-11-04 10:23:53.665734','129','FR',3,'',17,1),(179,'2018-11-04 10:23:53.710612','128','NATIONALITE',3,'',17,1),(180,'2018-11-04 10:23:53.755379','127','LIEU DE NAIS.',3,'',17,1),(181,'2018-11-04 10:23:53.799322','126','DATE NAIS.',3,'',17,1),(182,'2018-11-04 10:23:53.854343','125','SEXE',3,'',17,1),(183,'2018-11-04 10:23:53.922345','124','PRENOMS',3,'',17,1),(184,'2018-11-04 10:23:53.967037','123','NOM',3,'',17,1),(185,'2018-11-04 10:23:54.000621','122','ETABLISSEMENT',3,'',17,1),(186,'2018-11-04 10:23:54.033788','121','No  DE TABLE',3,'',17,1),(187,'2018-11-04 10:23:54.067225','120','No  ANONYMAT  1er GROUPE',3,'',17,1),(188,'2018-11-04 10:23:54.135959','119','Des/cout',3,'',17,1),(189,'2018-11-04 10:23:54.169871','118','EPS',3,'',17,1),(190,'2018-11-04 10:23:54.203337','117','LV2',3,'',17,1),(191,'2018-11-04 10:23:54.236147','116','MA',3,'',17,1),(192,'2018-11-04 10:23:54.269627','115','HG',3,'',17,1),(193,'2018-11-04 10:23:54.303053','114','LV1',3,'',17,1),(194,'2018-11-04 10:23:54.337176','113','PH',3,'',17,1),(195,'2018-11-04 10:23:54.381287','112','FR',3,'',17,1),(196,'2018-11-04 10:23:54.414589','111','NATIONALITE',3,'',17,1),(197,'2018-11-04 10:23:54.448358','110','LIEU DE NAIS.',3,'',17,1),(198,'2018-11-04 10:23:54.481871','109','DATE NAIS.',3,'',17,1),(199,'2018-11-04 10:23:54.515210','108','SEXE',3,'',17,1),(200,'2018-11-04 10:23:54.559856','107','PRENOMS',3,'',17,1),(201,'2018-11-04 10:24:15.456253','106','NOM',3,'',17,1),(202,'2018-11-04 10:24:15.533557','105','ETABLISSEMENT',3,'',17,1),(203,'2018-11-04 10:24:15.577714','104','No  DE TABLE',3,'',17,1),(204,'2018-11-04 10:24:15.622367','103','No  ANONYMAT  1er GROUPE',3,'',17,1),(205,'2018-11-04 10:24:15.666790','102','Des/cout',3,'',17,1),(206,'2018-11-04 10:24:15.734182','101','EPS',3,'',17,1),(207,'2018-11-04 10:24:15.789383','100','LV2',3,'',17,1),(208,'2018-11-04 10:24:15.833797','99','MA',3,'',17,1),(209,'2018-11-04 10:24:15.878232','98','HG',3,'',17,1),(210,'2018-11-04 10:24:15.922813','97','LV1',3,'',17,1),(211,'2018-11-04 10:24:15.967465','96','PH',3,'',17,1),(212,'2018-11-04 10:24:16.011458','95','FR',3,'',17,1),(213,'2018-11-04 10:24:16.045185','94','NATIONALITE',3,'',17,1),(214,'2018-11-04 10:24:16.078560','93','LIEU DE NAIS.',3,'',17,1),(215,'2018-11-04 10:24:16.111843','92','DATE NAIS.',3,'',17,1),(216,'2018-11-04 10:24:16.145317','91','SEXE',3,'',17,1),(217,'2018-11-04 10:24:16.178823','90','PRENOMS',3,'',17,1),(218,'2018-11-04 10:24:16.212068','89','NOM',3,'',17,1),(219,'2018-11-04 10:24:16.245780','88','ETABLISSEMENT',3,'',17,1),(220,'2018-11-04 10:24:16.312777','87','No  DE TABLE',3,'',17,1),(221,'2018-11-04 10:24:16.345640','86','No  ANONYMAT  1er GROUPE',3,'',17,1),(222,'2018-11-04 10:24:16.379101','85','Des/cout',3,'',17,1),(223,'2018-11-04 10:24:16.412430','84','EPS',3,'',17,1),(224,'2018-11-04 10:24:16.445909','83','LV2',3,'',17,1),(225,'2018-11-04 10:24:16.479678','82','MA',3,'',17,1),(226,'2018-11-04 10:24:16.513253','81','HG',3,'',17,1),(227,'2018-11-04 10:24:16.546658','80','LV1',3,'',17,1),(228,'2018-11-04 10:24:16.579741','79','PH',3,'',17,1),(229,'2018-11-04 10:24:16.613295','78','FR',3,'',17,1),(230,'2018-11-04 10:24:16.646835','77','NATIONALITE',3,'',17,1),(231,'2018-11-04 10:24:16.680285','76','LIEU DE NAIS.',3,'',17,1),(232,'2018-11-04 10:24:16.713899','75','DATE NAIS.',3,'',17,1),(233,'2018-11-04 10:24:16.747034','74','SEXE',3,'',17,1),(234,'2018-11-04 10:24:16.781571','73','PRENOMS',3,'',17,1),(235,'2018-11-04 10:24:16.804816','72','NOM',3,'',17,1),(236,'2018-11-04 10:24:16.848914','71','ETABLISSEMENT',3,'',17,1),(237,'2018-11-04 10:24:16.882417','70','No  DE TABLE',3,'',17,1),(238,'2018-11-04 10:24:16.916174','69','No  ANONYMAT  1er GROUPE',3,'',17,1),(239,'2018-11-04 10:24:16.949308','68','Des/cout',3,'',17,1),(240,'2018-11-04 10:24:16.982687','67','EPS',3,'',17,1),(241,'2018-11-04 10:24:17.016547','66','LV2',3,'',17,1),(242,'2018-11-04 10:24:17.049539','65','MA',3,'',17,1),(243,'2018-11-04 10:24:17.083072','64','HG',3,'',17,1),(244,'2018-11-04 10:24:17.116411','63','LV1',3,'',17,1),(245,'2018-11-04 10:24:17.149739','62','PH',3,'',17,1),(246,'2018-11-04 10:24:17.195015','61','FR',3,'',17,1),(247,'2018-11-04 10:24:17.239194','60','NATIONALITE',3,'',17,1),(248,'2018-11-04 10:24:17.283706','59','LIEU DE NAIS.',3,'',17,1),(249,'2018-11-04 10:24:17.327947','58','DATE NAIS.',3,'',17,1),(250,'2018-11-04 10:24:17.373134','57','SEXE',3,'',17,1),(251,'2018-11-04 10:24:17.417483','56','PRENOMS',3,'',17,1),(252,'2018-11-04 10:24:17.461670','55','NOM',3,'',17,1),(253,'2018-11-04 10:24:17.506229','54','ETABLISSEMENT',3,'',17,1),(254,'2018-11-04 10:24:17.551431','53','No  DE TABLE',3,'',17,1),(255,'2018-11-04 10:24:17.595477','52','No  ANONYMAT  1er GROUPE',3,'',17,1),(256,'2018-11-04 10:24:17.639752','51','Des/cout',3,'',17,1),(257,'2018-11-04 10:24:17.684507','50','EPS',3,'',17,1),(258,'2018-11-04 10:24:17.728818','49','LV2',3,'',17,1),(259,'2018-11-04 10:24:17.773440','48','MA',3,'',17,1),(260,'2018-11-04 10:24:17.817877','47','HG',3,'',17,1),(261,'2018-11-04 10:24:17.863220','46','LV1',3,'',17,1),(262,'2018-11-04 10:24:17.906937','45','PH',3,'',17,1),(263,'2018-11-04 10:24:17.952020','44','FR',3,'',17,1),(264,'2018-11-04 10:24:17.996450','43','NATIONALITE',3,'',17,1),(265,'2018-11-04 10:24:18.040465','42','LIEU DE NAIS.',3,'',17,1),(266,'2018-11-04 10:24:18.085253','41','DATE NAIS.',3,'',17,1),(267,'2018-11-04 10:24:18.129787','40','SEXE',3,'',17,1),(268,'2018-11-04 10:24:18.174377','39','PRENOMS',3,'',17,1),(269,'2018-11-04 10:24:18.219271','38','NOM',3,'',17,1),(270,'2018-11-04 10:24:18.263680','37','ETABLISSEMENT',3,'',17,1),(271,'2018-11-04 10:24:18.308156','36','No  DE TABLE',3,'',17,1),(272,'2018-11-04 10:24:18.352789','35','No  ANONYMAT  1er GROUPE',3,'',17,1),(273,'2018-11-04 10:24:18.396704','34','Des/cout',3,'',17,1),(274,'2018-11-04 10:24:18.441544','33','EPS',3,'',17,1),(275,'2018-11-04 10:24:18.485922','32','LV2',3,'',17,1),(276,'2018-11-04 10:24:18.530779','31','MA',3,'',17,1),(277,'2018-11-04 10:24:18.574974','30','HG',3,'',17,1),(278,'2018-11-04 10:24:18.619922','29','LV1',3,'',17,1),(279,'2018-11-04 10:24:18.664050','28','PH',3,'',17,1),(280,'2018-11-04 10:24:18.742860','27','FR',3,'',17,1),(281,'2018-11-04 10:24:18.786980','26','NATIONALITE',3,'',17,1),(282,'2018-11-04 10:24:18.831677','25','LIEU DE NAIS.',3,'',17,1),(283,'2018-11-04 10:24:18.875606','24','DATE NAIS.',3,'',17,1),(284,'2018-11-04 10:24:18.920307','23','SEXE',3,'',17,1),(285,'2018-11-04 10:24:18.965019','22','PRENOMS',3,'',17,1),(286,'2018-11-04 10:24:19.009409','21','NOM',3,'',17,1),(287,'2018-11-04 10:24:19.053786','20','ETABLISSEMENT',3,'',17,1),(288,'2018-11-04 10:24:19.098854','19','No  DE TABLE',3,'',17,1),(289,'2018-11-04 10:24:19.143276','18','No  ANONYMAT  1er GROUPE',3,'',17,1),(290,'2018-11-04 10:24:19.203281','17','Des/cout',3,'',17,1),(291,'2018-11-04 10:24:19.276480','16','EPS',3,'',17,1),(292,'2018-11-04 10:24:19.354541','15','LV2',3,'',17,1),(293,'2018-11-04 10:24:19.399167','14','MA',3,'',17,1),(294,'2018-11-04 10:24:19.443521','13','HG',3,'',17,1),(295,'2018-11-04 10:24:19.488153','12','LV1',3,'',17,1),(296,'2018-11-04 10:24:19.532574','11','PH',3,'',17,1),(297,'2018-11-04 10:24:19.621888','10','FR',3,'',17,1),(298,'2018-11-04 10:24:19.667036','9','NATIONALITE',3,'',17,1),(299,'2018-11-04 10:24:19.712050','8','LIEU DE NAIS.',3,'',17,1),(300,'2018-11-04 10:24:19.755784','7','DATE NAIS.',3,'',17,1),(301,'2018-11-04 10:24:34.135030','6','SEXE',3,'',17,1),(302,'2018-11-04 10:24:34.197204','5','PRENOMS',3,'',17,1),(303,'2018-11-04 10:24:34.242053','4','NOM',3,'',17,1),(304,'2018-11-04 10:24:34.286773','3','ETABLISSEMENT',3,'',17,1),(305,'2018-11-04 10:24:34.331261','2','No  DE TABLE',3,'',17,1),(306,'2018-11-04 10:24:34.375453','1','No  ANONYMAT  1er GROUPE',3,'',17,1),(307,'2018-11-04 10:30:55.918558','340','LV1',3,'',17,1),(308,'2018-11-04 10:30:55.991217','339','LV2',3,'',17,1),(309,'2018-11-04 10:30:56.035484','338','PH',3,'',17,1),(310,'2018-11-04 10:30:56.080142','337','Des/cout',3,'',17,1),(311,'2018-11-04 10:30:56.124909','336','EPS',3,'',17,1),(312,'2018-11-04 10:30:56.169010','335','HG',3,'',17,1),(313,'2018-11-04 10:30:56.213739','334','MA',3,'',17,1),(314,'2018-11-04 10:30:56.258093','333','FR',3,'',17,1),(315,'2018-11-04 10:30:56.315299','332','NATIONALITE',3,'',17,1),(316,'2018-11-04 10:30:56.380832','331','SEXE',3,'',17,1),(317,'2018-11-04 10:30:56.436741','330','PRENOMS',3,'',17,1),(318,'2018-11-04 10:30:56.481281','329','LIEU DE NAIS.',3,'',17,1),(319,'2018-11-04 10:30:56.525353','328','DATE NAIS.',3,'',17,1),(320,'2018-11-04 10:30:56.570349','327','No  DE TABLE',3,'',17,1),(321,'2018-11-04 10:30:56.614312','326','No  ANONYMAT  1er GROUPE',3,'',17,1),(322,'2018-11-04 10:30:56.661077','325','NOM',3,'',17,1),(323,'2018-11-04 10:30:56.705778','324','ETABLISSEMENT',3,'',17,1),(324,'2018-11-04 10:30:56.750244','323','LV1',3,'',17,1),(325,'2018-11-04 10:30:56.794770','322','LV2',3,'',17,1),(326,'2018-11-04 10:30:56.839637','321','PH',3,'',17,1),(327,'2018-11-04 10:30:56.883851','320','Des/cout',3,'',17,1),(328,'2018-11-04 10:30:56.928561','319','EPS',3,'',17,1),(329,'2018-11-04 10:30:56.973691','318','HG',3,'',17,1),(330,'2018-11-04 10:30:57.017778','317','MA',3,'',17,1),(331,'2018-11-04 10:30:57.062045','316','FR',3,'',17,1),(332,'2018-11-04 10:30:57.106573','315','NATIONALITE',3,'',17,1),(333,'2018-11-04 10:30:57.151316','314','SEXE',3,'',17,1),(334,'2018-11-04 10:30:57.195556','313','PRENOMS',3,'',17,1),(335,'2018-11-04 10:30:57.240182','312','LIEU DE NAIS.',3,'',17,1),(336,'2018-11-04 10:30:57.284774','311','DATE NAIS.',3,'',17,1),(337,'2018-11-04 10:30:57.429292','310','No  DE TABLE',3,'',17,1),(338,'2018-11-04 10:30:57.462625','309','No  ANONYMAT  1er GROUPE',3,'',17,1),(339,'2018-11-04 10:30:57.496318','308','NOM',3,'',17,1),(340,'2018-11-04 10:30:57.529353','307','ETABLISSEMENT',3,'',17,1),(341,'2018-11-04 16:27:10.846623','3','Points object',2,'[]',18,1),(342,'2018-11-04 16:30:14.480784','3','Points object',2,'[{\"changed\": {\"fields\": [\"Groupe_2\"]}}]',18,1),(343,'2018-11-04 16:30:26.398801','3','Points object',2,'[{\"changed\": {\"fields\": [\"Groupe_2\"]}}]',18,1),(344,'2018-11-04 23:16:36.153114','4','D	|	Grpe 1:160	|	Grpe 2:140',1,'[{\"added\": {}}]',18,1),(345,'2018-11-05 02:11:20.661517','1','Admin object',2,'[{\"changed\": {\"fields\": [\"password\"]}}]',7,1),(346,'2018-11-05 07:30:31.356137','kachaloali','kachaloali',2,'[{\"changed\": {\"fields\": [\"password\"]}}]',8,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(2,'auth','group'),(3,'auth','permission'),(4,'auth','user'),(7,'calculator','admin'),(13,'calculator','centre'),(17,'calculator','csvallcols'),(16,'calculator','csvheader'),(11,'calculator','coefficient'),(14,'calculator','jury'),(9,'calculator','matiere'),(18,'calculator','points'),(12,'calculator','region'),(15,'calculator','serie'),(10,'calculator','student'),(19,'calculator','typescols'),(8,'calculator','user'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-11-04 00:47:07.619802'),(2,'auth','0001_initial','2018-11-04 00:47:17.843376'),(3,'admin','0001_initial','2018-11-04 00:47:19.945882'),(4,'admin','0002_logentry_remove_auto_add','2018-11-04 00:47:20.146381'),(5,'contenttypes','0002_remove_content_type_name','2018-11-04 00:47:21.255653'),(6,'auth','0002_alter_permission_name_max_length','2018-11-04 00:47:21.386369'),(7,'auth','0003_alter_user_email_max_length','2018-11-04 00:47:21.568139'),(8,'auth','0004_alter_user_username_opts','2018-11-04 00:47:21.655492'),(9,'auth','0005_alter_user_last_login_null','2018-11-04 00:47:22.385636'),(10,'auth','0006_require_contenttypes_0002','2018-11-04 00:47:22.429986'),(11,'auth','0007_alter_validators_add_error_messages','2018-11-04 00:47:22.661476'),(12,'auth','0008_alter_user_username_max_length','2018-11-04 00:47:23.477206'),(13,'calculator','0001_initial','2018-11-04 00:47:23.932021'),(14,'sessions','0001_initial','2018-11-04 00:47:24.488962'),(15,'calculator','0002_auto_20181104_0304','2018-11-04 02:04:54.114914'),(16,'calculator','0003_auto_20181104_1120','2018-11-04 10:21:13.887899'),(17,'calculator','0004_auto_20181104_1716','2018-11-04 16:16:14.173518'),(18,'calculator','0005_auto_20181104_1725','2018-11-04 16:25:19.064634'),(19,'calculator','0006_points_codeserie','2018-11-04 21:20:50.580757');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('fjjhpmlmer4x6qcn690qjglnasp3eodm','YWY1ZWYyMzM3YzM5MGNlMjBlNDYyMTg0MWQ3ZGU2ODM2MzdmYzk2Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjA0Y2M4OTYxMDUxNzUwZjAyNGExZGM3ZjE5YTZkODAyYTBhMGM0NjAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-11-19 08:27:31.592593');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coefficient`
--

DROP TABLE IF EXISTS `coefficient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coefficient` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `codeserie` varchar(10) NOT NULL,
  `codemat` varchar(10) NOT NULL,
  `coefmat` int(10) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `codeserie` (`codeserie`),
  CONSTRAINT `coefficient_ibfk_1` FOREIGN KEY (`codeserie`) REFERENCES `serie` (`codeserie`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coefficient`
--

LOCK TABLES `coefficient` WRITE;
/*!40000 ALTER TABLE `coefficient` DISABLE KEYS */;
INSERT INTO `coefficient` VALUES (1,'A4','Ang',3),(2,'D','Ang',2),(3,'A4','FR',4),(4,'A4','HG',4),(5,'A4','MA',1),(6,'A4','LV1',3),(7,'A4','LV2',3),(8,'A4','PHILO',4),(9,'A4','EPS',1),(10,'A4','DESS',1),(11,'A4','COUT',1);
/*!40000 ALTER TABLE `coefficient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jury`
--

DROP TABLE IF EXISTS `jury`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jury` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nojury` int(11) NOT NULL,
  `centre` varchar(50) NOT NULL,
  `region` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `region` (`region`),
  CONSTRAINT `jury_ibfk_1` FOREIGN KEY (`region`) REFERENCES `region` (`nameregion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jury`
--

LOCK TABLES `jury` WRITE;
/*!40000 ALTER TABLE `jury` DISABLE KEYS */;
INSERT INTO `jury` VALUES (1,1,'Dan Koulodo','Maradi'),(2,2,'Illimi','Maradi'),(3,2,'LDB','Maradi'),(4,1,'LDB','Maradi'),(5,3,'LDB','Maradi');
/*!40000 ALTER TABLE `jury` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matiere` (
  `codemat` varchar(10) NOT NULL,
  `matiere` varchar(50) NOT NULL,
  PRIMARY KEY (`codemat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matiere`
--

LOCK TABLES `matiere` WRITE;
/*!40000 ALTER TABLE `matiere` DISABLE KEYS */;
INSERT INTO `matiere` VALUES ('Ang','Anglais'),('COUT','Couture'),('DESS','Dessin'),('EPS','Éducation Physique et Sportive'),('FR','français'),('HG','Histoire-Géographie'),('LV1','Langue vivante 1'),('LV2','Langue vivante 2'),('MA','Mathématiques'),('PH','Philosophie');
/*!40000 ALTER TABLE `matiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `nameregion` varchar(50) NOT NULL,
  PRIMARY KEY (`nameregion`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES ('Maradi'),('Zinder'),('Tahoua'),('Agadez'),('Niamey'),('Tillaberi'),('Dosso'),('Diffa');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serie`
--

DROP TABLE IF EXISTS `serie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serie` (
  `codeserie` varchar(10) NOT NULL,
  PRIMARY KEY (`codeserie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serie`
--

LOCK TABLES `serie` WRITE;
/*!40000 ALTER TABLE `serie` DISABLE KEYS */;
INSERT INTO `serie` VALUES ('A4'),('C'),('D'),('E'),('F1'),('F2'),('F4'),('G1'),('G2'),('G3');
/*!40000 ALTER TABLE `serie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `anonymat` int(10) NOT NULL,
  `notable` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `datenaiss` varchar(20) NOT NULL,
  `lieunaiss` varchar(50) NOT NULL,
  `nationalite` varchar(20) NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `etablissement` varchar(50) NOT NULL,
  PRIMARY KEY (`notable`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `region` varchar(50) NOT NULL,
  `numero` int(10) NOT NULL,
  `centre` varchar(50) NOT NULL,
  `codeserie` varchar(10) NOT NULL,
  PRIMARY KEY (`login`),
  KEY `region` (`region`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`region`) REFERENCES `region` (`nameregion`),
  KEY `codeserie` (`codeserie`),
  CONSTRAINT `jury_ibfk_2` FOREIGN KEY (`codeserie`) REFERENCES `serie` (`codeserie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('Ali','Kachalo','kachaloali','2018','Maradi',1,'Dan Koulodo','A4'),('Abdou Arbi','Oumarou','oumarouarbi','admin','Maradi',1,'LDB','D');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-05  9:46:14
